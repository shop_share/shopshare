import 'package:flutter/material.dart';
import 'package:shop_share/create/add_item_dialog.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/services/sqlite_handler.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/item_tiles.dart';

/// ItemOverviewCreate
///
/// Item overview widget for shopping lists called from the create component.
/// Includes editing features like adding new items oder changing them.
class ItemOverviewCreate extends StatefulWidget {
  final ShoppingList shoppingList;

  const ItemOverviewCreate(this.shoppingList, {Key? key}) : super(key: key);

  @override
  State<ItemOverviewCreate> createState() => _ItemOverviewCreateState();
}

class _ItemOverviewCreateState extends State<ItemOverviewCreate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.shoppingList.name),
      ),
      body: ItemTiles(widget.shoppingList, ComponentMode.create), //Main Widget: ItemTiles in tools-package
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) => AddItemDialog(
                title: "Add new item",
                id: null,
                itemName: "",
                itemNote: "",
                amount: "1",
                itemExpiry: null,
                shoppingListId: widget.shoppingList.id,
                mode: ComponentMode.create)
          ).then((value) => setState(() {
                widget.shoppingList.items.add(value);
                SqliteHandler.insertItem(value);
              }));
        },
        tooltip: 'Create new item',
        child: const Icon(Icons.add),
      ),
    );
  }
}
