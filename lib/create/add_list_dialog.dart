import 'package:flutter/material.dart';
import 'package:shop_share/entities/shopping_list.dart';

/// AddListDialog
/// 
/// Dialog to add new shopping lists to local db.
class AddListDialog extends StatefulWidget {
  const AddListDialog({Key? key}) : super(key: key);

  @override
  State<AddListDialog> createState() => _AddListDialogState();
}

class _AddListDialogState extends State<AddListDialog> {
  final String title = "Create new shopping list";
  final String content = "";
  final List<Widget> actions = [];
  TextEditingController listNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
        title: Text(
          title,
        ),
        actions: actions,
        content: Center(
          child: Column(
            children: [
              TextFormField(
                controller: listNameController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.shopping_basket_sharp),
                  labelText: 'List Name',
                  labelStyle: TextStyle(
                    color: Color(0xFF6200EE),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF6200EE)),
                  ),
                ),
              ),
              const SizedBox(height: 30),
              ElevatedButton.icon(
                onPressed: () {
                  ShoppingList newList = ShoppingList(listNameController.text);
                  Navigator.pop(context, newList);
                },
                icon: const Icon(Icons.add, size: 18),
                label: const Text("Add List"),
              )
            ],
          ),
        ));
  }
}
