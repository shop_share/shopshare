import 'package:flutter/material.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/shoppinglist_tiles.dart';

import '../services/sqlite_handler.dart';
import 'add_list_dialog.dart';

/// CreateOverview
///
/// Overview widget for create component of map. Shows all local shopping lists.
class CreateOverview extends StatefulWidget {
  const CreateOverview({Key? key}) : super(key: key);

  @override
  State<CreateOverview> createState() => _CreateOverviewState();
}

class _CreateOverviewState extends State<CreateOverview>
    with AutomaticKeepAliveClientMixin<CreateOverview> {
  @override
  bool get wantKeepAlive => true;

  bool retrievedListsFromDb = false;

  List<ShoppingList> shoppingLists = [];

  _CreateOverviewState() {
    SqliteHandler.createTables();
    _getShoppingLists();
  }

  /// getShoppingLists
  ///
  /// Retrieve all local shopping lists from sqlite db
  _getShoppingLists() async {
    shoppingLists = await SqliteHandler.getShoppingLists();
    setState(() {
      retrievedListsFromDb = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (retrievedListsFromDb) {
      return Scaffold(
        body: ShoppingListTiles(shoppingLists, ComponentMode.create),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) => const AddListDialog(),
            ).then((value) => setState(() {
              shoppingLists.add(value);
              SqliteHandler.insertShoppingList(value);
            }));
          },
          tooltip: 'Create new shopping list',
          child: const Icon(Icons.add),
        ),
      );
    } else {
      return const Scaffold();
    }
  }
}
