import 'package:flutter/material.dart';
import 'package:shop_share/tools/component_mode.dart';

import '../entities/item.dart';

/// AddItemDialog
///
/// Dialog to add or change items.
/// Can also be called from share components in readonly mode.
class AddItemDialog extends StatefulWidget {
  final String title;
  final String itemName;
  final String itemNote;
  final String? id;
  final DateTime? itemExpiry;
  final String amount;
  final String shoppingListId;
  final ComponentMode mode;

  const AddItemDialog(
      {Key? key,
      required this.title,
      required this.itemName,
      required this.itemNote,
      required this.id,
      required this.amount,
      required this.itemExpiry,
      required this.shoppingListId,
      required this.mode})
      : super(key: key);

  @override
  State<AddItemDialog> createState() => _AddItemDialogState();
}

class _AddItemDialogState extends State<AddItemDialog> {
  final String content = "";
  final List<Widget> actions = [];
  TextEditingController itemNameController = TextEditingController();
  TextEditingController noteController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController amountController = TextEditingController();

  bool _expiry = false;
  int _groupValue = 0;

  /// Add information about an existing item into the text fields
  @override
  void initState() {
    super.initState();

    if (widget.itemExpiry == null) {
      _expiry = false;
      _groupValue = 1;
    } else {
      _expiry = true;
      _groupValue = 0;
    }

    itemNameController.text = widget.itemName;
    noteController.text = widget.itemNote;
    amountController.text = widget.amount;
    dateController.text = widget.itemExpiry == null ? DateTime.now().toString().substring(0,10) :
    widget.itemExpiry.toString().substring(0, 10);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        scrollable: true,
        title: Text(
          widget.title,
        ),
        actions: actions,
        content: Center(
          child: Column(
            children: [
              TextFormField(
                controller: itemNameController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.shopping_basket_sharp),
                  labelText: 'Item Name',
                  labelStyle: TextStyle(
                    color: Color(0xFF6200EE),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF6200EE)),
                  ),
                ),
                readOnly: widget.mode == ComponentMode.share,
              ),
              TextFormField(
                controller: amountController,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  icon: Icon(Icons.numbers),
                  labelText: 'Amount',
                  labelStyle: TextStyle(
                    color: Color(0xFF6200EE),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF6200EE)),
                  ),
                ),
                readOnly: widget.mode == ComponentMode.share,
              ),
              TextFormField(
                controller: noteController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.edit),
                  labelText: 'Note',
                  labelStyle: TextStyle(
                    color: Color(0xFF6200EE),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF6200EE)),
                  ),
                ),
                readOnly: widget.mode == ComponentMode.share,
              ),
              SizedBox(height: 50,),
              Text("Expiry"),
              Row(
                children: [
                  Flexible(
                    child: ListTile(
                      title: const Text("yes"),
                      leading: Radio(
                        value: 0,
                        groupValue: _groupValue,
                        onChanged: (int? val) {
                          setState(() {
                            _expiry = true;
                            _groupValue = val!;
                          });
                        },
                      ),
                    ),
                  ),
                  Flexible(
                    child: ListTile(
                      title: const Text("no"),
                      leading: Radio(
                        value: 1,
                        groupValue: _groupValue,
                        onChanged: (int? val) {
                          setState(() {
                            _groupValue = val!;
                            _expiry = false;
                          });
                        },
                      ),
                    ),
                  )

                ],
              ),
              if (_expiry) ...[
                TextField(
                    readOnly: true,
                    controller: dateController,
                    decoration: InputDecoration(
                        hintText: 'Maximum expiry of item',
                        icon: Icon(Icons.timer)),
                    onTap: () async {
                      if (widget.mode == ComponentMode.create) {
                        var date = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100));
                        dateController.text = date.toString().substring(0, 10);
                      }
                    })
              ],
              if (widget.mode == ComponentMode.create) ...[
                ElevatedButton.icon(
                  onPressed: () {
                    if (widget.id == null) {
                      Item newItem = Item(
                          name: itemNameController.text,
                          amount: int.parse(amountController.text),
                          expiry: _expiry ? DateTime.parse(dateController.text) : null,
                          note: noteController.text,
                          shoppingListId: widget.shoppingListId);
                      Navigator.pop(context, newItem);
                    } else {
                      Item newItem = Item(
                          id: widget.id,
                          name: itemNameController.text,
                          amount: int.parse(amountController.text),
                          expiry: _expiry ? DateTime.parse(dateController.text) : null,
                          note: noteController.text,
                          shoppingListId: widget.shoppingListId);
                      Navigator.pop(context, newItem);
                    }
                  },
                  icon: const Icon(Icons.add, size: 18),
                  label: const Text("Add Item"),
                )
              ]
            ],
          ),
        ));
  }
}
