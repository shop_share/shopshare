import 'package:flutter/material.dart';
import 'package:shop_share/entities/shopping_list.dart';

/// EditListDialog
///
/// Dialog to edit existing lists.
class EditListDialog extends StatefulWidget {
  final ShoppingList shoppingList;

  const EditListDialog({Key? key, required this.shoppingList})
      : super(key: key);

  @override
  State<EditListDialog> createState() => _EditListDialogState();
}

class _EditListDialogState extends State<EditListDialog> {
  final String title = "Edit shopping list";
  final String content = "";
  final List<Widget> actions = [];
  TextEditingController listNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    listNameController.text = widget.shoppingList.name;
    return AlertDialog(
        scrollable: true,
        title: Text(
          title,
        ),
        actions: actions,
        content: Center(
          child: Column(
            children: [
              TextFormField(
                controller: listNameController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.shopping_basket_sharp),
                  labelText: 'List Name',
                  labelStyle: TextStyle(
                    color: Color(0xFF6200EE),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF6200EE)),
                  ),
                ),
              ),
              ElevatedButton.icon(
                onPressed: () {
                  widget.shoppingList.name = listNameController.text;
                  Navigator.pop(context, widget.shoppingList);
                },
                icon: const Icon(Icons.add, size: 18),
                label: const Text("Change name"),
              )
            ],
          ),
        ));
  }
}
