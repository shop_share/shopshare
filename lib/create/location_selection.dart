import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

/// LocationSelection
///
/// Widget to select a delivery location for a to be shared shopping list.
class LocationSelection extends StatefulWidget {
  const LocationSelection({Key? key}) : super(key: key);

  @override
  State<LocationSelection> createState() => _LocationSelectionState();
}

class _LocationSelectionState extends State<LocationSelection> {

  late GoogleMapController mapController;
  late LatLng _center;
  Location currentLocation = Location();
  List<Marker> _markers = [];

  ///Set Standard location, if gps cannot be retrieved.
  _LocationSelectionState() {
    _center = const LatLng(50.569480444013486, 9.997810808064006);
    _getCenter();
  }

  /// _getCenter
  ///
  /// Get current location and move camera to it.
  _getCenter() async {
    var currentLoc = await currentLocation.getLocation();
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(currentLoc.latitude ?? 0.0,currentLoc.longitude?? 0.0),
      zoom: 12.0,
    )));

  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  _finishLocationInput() {
    Navigator.pop(context, _markers[0].position);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Zielort auswählen"),
        actions: [
          IconButton(
              onPressed: _finishLocationInput,
              icon: const Icon(Icons.done))
        ],
      ),
      body: GoogleMap(
        zoomControlsEnabled: false,
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 8.0,
        ),
        markers: _markers.toSet(),
        onTap: (loc) {
          var marker = Marker(markerId: MarkerId("Dest"),
              position: loc);
          setState(() {
            _markers = [];
            _markers.add(marker);
          });
        },
      ),
    );
  }
}
