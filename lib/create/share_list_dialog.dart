import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shop_share/entities/shopping_list.dart';

import '../entities/shared_shopping_list.dart';
import 'location_selection.dart';
import 'package:http/http.dart' as http;

/// ShareListDialog
///
/// Dialog to share a local shopping list and add additional information for assignees.
class ShareListDialog extends StatefulWidget {
  final ShoppingList shoppingList;

  const ShareListDialog(this.shoppingList, {Key? key}) : super(key: key);

  @override
  State<ShareListDialog> createState() => _ShareListDialogState();
}

class _ShareListDialogState extends State<ShareListDialog> {
  final String title = "Share shopping list";
  final String content = "";
  final List<Widget> actions = [];
  TextEditingController textEditingController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  late DateTime currentDateTime;
  LatLng? location;

  /// getAddressNameOfCoordinates
  ///
  /// Uses Nominatim API to get address name of a coordinate returend by the LocationSelection widget.
  /// @param coordinates - Coordinates with latitude and longitude, to which the creator wants the list to be delivered.
  /// @return String with address to which the coordinates point
  Future<String> getAddressNameOfCoordinates(LatLng coordinates) async {
    try {
      http.Response res = await http.get(
          Uri.parse("https://nominatim.openstreetmap.org"
          "/reverse?lat=${coordinates.latitude.toString()}&"
              "lon=${coordinates.longitude.toString()}&format=json"));
      //final stringData = await response.transform(utf8.decoder).join();
      final Map<String, dynamic> jsonData = jsonDecode(res.body);
      if (jsonData.containsKey("address")) {
        final Map<String, dynamic> addressData = jsonData["address"];
        return "${addressData["road"]} ${addressData["house_number"]}, "
            "${addressData["postcode"]} ${addressData["city"]}, "
            "${addressData["country"]}";
      } else {
        return "Unknown location";
      }
    } catch (e) {
      return "Unknown location";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        scrollable: true,
        title: Text(
          title,
        ),
        actions: actions,
        content: Center(
          child: Column(
            children: [
              // TextFormField(
              //   controller: textEditingController,
              //   decoration: const InputDecoration(
              //     icon: Icon(Icons.edit_note),
              //     labelText: 'Note for supplier',
              //     labelStyle: TextStyle(
              //       color: Color(0xFF6200EE),
              //     ),
              //     enabledBorder: UnderlineInputBorder(
              //       borderSide: BorderSide(color: Color(0xFF6200EE)),
              //     ),
              //   ),
              // ),
              TextField(
                  readOnly: true,
                  controller: dateController,
                  decoration: const InputDecoration(
                      hintText: 'Expiry of shopping list'),
                  onTap: () async {
                    var date = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100));
                    currentDateTime = date!;
                    dateController.text = date.toString().substring(0, 10);
                  }),
              TextField(
                  readOnly: true,
                  controller: locationController,
                  decoration:
                      const InputDecoration(hintText: 'Delivery destination'),
                  onTap: () async {
                    var location = await Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const LocationSelection();
                    }));
                    if (location != null) {
                      this.location = location;
                      getAddressNameOfCoordinates(location).then((address) {
                        locationController.text = address;
                      });
                    }
                  }),
              ElevatedButton.icon(
                onPressed: () {
                  if (location == null) {
                    return;
                    //TODO: Fehlermeldung einfügen, wenn Location noch nicht gesetzt
                  }
                  SharedShoppingList newList = SharedShoppingList(
                      name: widget.shoppingList.name, expiry: currentDateTime);
                  newList.id = widget.shoppingList.id;
                  newList.items = widget.shoppingList.items;
                  newList.location =
                      GeoPoint(location!.latitude, location!.longitude);
                  Navigator.pop(context, newList);
                },
                icon: const Icon(Icons.share, size: 18),
                label: const Text("Share list"),
              )
            ],
          ),
        ));
  }
}
