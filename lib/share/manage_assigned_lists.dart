import 'package:flutter/material.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/shoppinglist_tiles.dart';

import '../entities/shared_shopping_list.dart';
import '../services/firestore_service.dart';

/// ManageAssignedLists
/// 
/// Dialog window where assigned lists can be viewed and managed
class ManageAssignedLists extends StatefulWidget {
  const ManageAssignedLists({Key? key}) : super(key: key);

  @override
  State<ManageAssignedLists> createState() => _ManageAssignedListsState();
}

class _ManageAssignedListsState extends State<ManageAssignedLists>
    with AutomaticKeepAliveClientMixin<ManageAssignedLists> {
  @override
  bool get wantKeepAlive => true;

  List<SharedShoppingList> shoppingLists = [];

  @override
  void initState() {
    super.initState();

    FirestoreService.getAssignedShoppingLists().then((lists) => {
      setState(() {
        shoppingLists = lists;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assigned lists"),
      ),
      body: ShoppingListTiles(shoppingLists, ComponentMode.share),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     showDialog(
      //       context: context,
      //       builder: (BuildContext context) => const AddListDialog(),
      //     ).then((value) => setState(() {
      //       shoppingLists.add(value);
      //     }));
      //   },
      //   tooltip: 'Create new shopping list',
      //   child: const Icon(Icons.add),
      // ),
    );
  }
}
