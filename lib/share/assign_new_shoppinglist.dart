import 'package:flutter/material.dart';
import 'package:shop_share/entities/shared_shopping_list.dart';
import 'package:shop_share/services/firestore_service.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/item_tiles.dart';

/// AssignNewShoppingList
///
/// Dialog to assign a shopping list
class AssignNewShoppingList extends StatefulWidget {
  final SharedShoppingList shoppingList;
  const AssignNewShoppingList(this.shoppingList, {Key? key}) : super(key: key);

  @override
  State<AssignNewShoppingList> createState() => _AssignNewShoppingListState();
}

class _AssignNewShoppingListState extends State<AssignNewShoppingList> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.shoppingList.name),
        actions: [
          IconButton(
              onPressed: () {
                  FirestoreService.assignNewShoppingList(widget.shoppingList);
                  Navigator.pop(context, true);
              },
              icon: const Icon(Icons.done))
        ],
      ),
      body: Center(
        child: Column(
          children: [
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.calendar_today),
                    title: const Text('Expiry'),
                    subtitle: Text(widget.shoppingList.expiry.toString().split(' ')[0]),
                  )
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.list),
                    title: const Text('Items')
                  ),
                  Container(
                    height: 400,
                    child: ItemTiles(widget.shoppingList, ComponentMode.share),
                  )

                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
