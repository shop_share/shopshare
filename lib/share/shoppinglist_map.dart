import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:shop_share/entities/shared_shopping_list.dart';
import 'package:shop_share/services/firestore_service.dart';
import 'package:shop_share/share/assign_new_shoppinglist.dart';

/// ShoppingListMap
/// 
/// Dialog showing shopping lists in the nearby area
class ShoppingListMap extends StatefulWidget {
  const ShoppingListMap({Key? key}) : super(key: key);

  @override
  State<ShoppingListMap> createState() => _ShoppingListMapState();
}

class _ShoppingListMapState extends State<ShoppingListMap> {
  late GoogleMapController mapController;
  List<Marker> _markers = [];
  Location currentLocation = Location();
  late BitmapDescriptor pinLocationIcon;

  List<SharedShoppingList> pulledLists = [];

  /// getLocation
  /// 
  /// Fetches the current location of the user
  void getLocation() async {
    var location = await currentLocation.getLocation();
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(location.latitude ?? 0.0, location.longitude ?? 0.0),
      zoom: 15.0,
    )));
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId('Home'),
        position: LatLng(location.latitude ?? 0.0, location.longitude ?? 0.0),
        icon: pinLocationIcon,
      ));
    });
    getShoppingListsInArea(LatLng(location.latitude!, location.longitude!));
  }

  /// getShoppingListsInArea
  /// 
  /// Method fetching all shopping lists nearby
  void getShoppingListsInArea(LatLng curLoc) async {
    setState(() async{
      pulledLists = await FirestoreService.getShoppingListsAtCoordinate(curLoc);
      _generateMarkers();
    });
  }

  /// generateMarkers
  /// 
  /// Places markers for all shopping lists nearby
  void _generateMarkers() {
    for (var list in pulledLists) {
      setState(() {
        _markers.add(
            Marker(
                markerId: MarkerId(list.id.toString()),
                position: LatLng(list.location.latitude, list.location.longitude),
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute<bool>(builder: (context) {
                    return AssignNewShoppingList(list);
                  })).then((value) {
                    if (value != null && value) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              behavior: SnackBarBehavior.floating,
                              content: Text('Shopping list was assigned!')
                          )
                      );
                      setState(() {
                        pulledLists.remove(list);
                        _markers = [];
                        getLocation();
                      });
                    }
                  });
                  setState(() {
                    _markers = [];
                    getLocation();
                  });
                }
            )
        );
      });
    }
  }

  _ShoppingListMapState() {
    _markers.clear();
  }

  final LatLng _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  /// initState
  /// 
  /// Loads map, places markers and zooms into correct position
  @override
  void initState() {
    super.initState();
    BitmapDescriptor.fromAssetImage(
            const ImageConfiguration(devicePixelRatio: 2.5),
            'assets/person_pin_circle_FILL0_wght400_GRAD0_opsz48.png')
        .then((value) {
      pinLocationIcon = value;
    });
    setState(() {
      getLocation();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lists in your area"),
      ),
      body: GoogleMap(
        zoomControlsEnabled: false,
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 11.0,
        ),
        markers: _markers.toSet(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          getLocation();
          // var location = await currentLocation.getLocation();
          // mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          //   target: LatLng(location.latitude ?? 0.0, location.longitude ?? 0.0),
          //   zoom: 15.0,
          // )));
          // setState(() {
          //   _markers.add(Marker(
          //     markerId: MarkerId('Home'),
          //     position: LatLng(location.latitude ?? 0.0, location.longitude ?? 0.0),
          //     icon: pinLocationIcon,
          //   ));
          // });
        },
        tooltip: 'Go to your current position',
        child: const Icon(Icons.gps_fixed),
      ),
    );
  }
}
