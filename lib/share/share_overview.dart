import 'package:flutter/material.dart';
import 'package:shop_share/share/live_shopping_mode.dart';
import 'package:shop_share/share/manage_assigned_lists.dart';
import 'package:shop_share/share/manage_created_lists.dart';
import 'package:shop_share/share/shoppinglist_map.dart';

/// ShareOverview
/// 
/// Main window from share window
class ShareOverview extends StatefulWidget {
  const ShareOverview({Key? key}) : super(key: key);

  @override
  _ShareOverviewState createState() => _ShareOverviewState();
}

class _ShareOverviewState extends State<ShareOverview> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          GestureDetector(
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.playlist_add),
                    title: const Text('Collect'),
                    subtitle: Text(
                      'Collect new shopping lists in your area!',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  )
                ],
              ),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const ShoppingListMap();
              }));
            },
          ),
          GestureDetector(
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.manage_accounts),
                    title: const Text('Manage'),
                    subtitle: Text(
                      'Manage your tools and assigned shopping lists!',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  )
                ],
              ),
            ),
            onTap: () {
              //Zur ShoppingList-Verwaltung
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const ManageAssignedLists();
              }));
            },
          ),
          GestureDetector(
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.shopping_cart),
                    title: const Text('Go Shopping'),
                    subtitle: Text(
                      'Go shopping and treat someone something good!',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  )
                ],
              ),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LiveShoppingMode();
              }));
            },
          ),
          GestureDetector(
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.edit),
                    title: const Text('Manage created'),
                    subtitle: Text(
                      'Manage created shopping lists!',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  )
                ],
              ),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const ManageCreatedLists();
              }));
            },
          )
        ],
      )
    );
  }
}
