import 'package:flutter/material.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/item_tiles.dart';

import '../entities/shared_shopping_list.dart';
import '../services/firestore_service.dart';


/// LiveShoppingMode
/// 
/// View of all items of all assigned shopping lists
class LiveShoppingMode extends StatefulWidget {
  const LiveShoppingMode({Key? key}) : super(key: key);

  @override
  State<LiveShoppingMode> createState() => _LiveShoppingModeState();
}

class _LiveShoppingModeState extends State<LiveShoppingMode> {

  List<SharedShoppingList> allShoppingLists = [];
  bool fetchedLists = false;
  ShoppingList mergedList = ShoppingList("temp");


  _LiveShoppingModeState() {
    FirestoreService.getAssignedShoppingLists();
  }

  /// initState
  /// 
  /// Fetches all assigned shopping lists and their respective items
  @override
  initState() {
    super.initState();
    FirestoreService.getAssignedShoppingLists().then((value) {
      var onlyList = _mergeLists(value);
      setState(() {
        mergedList = onlyList;
        fetchedLists = true;
      });
    });
  }

/// mergeLists
/// 
/// Merges multiple lists into a single one
/// @param: lists - List of shopping lists that will be merged
/// @return List containing all of the items of the individual shopping lists
  SharedShoppingList _mergeLists(List<SharedShoppingList> lists) {
    SharedShoppingList mergedList = SharedShoppingList(name: "merged", expiry: DateTime.now());
    for (var list in lists) {
      mergedList.items.addAll(list.items);
    }
    return mergedList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Shopping List"),
      ),
      body: fetchedLists ? ItemTiles(mergedList, ComponentMode.share) : null,
    );
  }
}
