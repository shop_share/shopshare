import 'package:flutter/material.dart';
import 'package:shop_share/create/add_item_dialog.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/item_tiles.dart';

/// ItemOverviewShare
///
/// View of all of the items in a shopping list without editing or creation features
class ItemOverviewShare extends StatefulWidget {
  final ShoppingList shoppingList;

  const ItemOverviewShare(this.shoppingList, {Key? key}) : super(key: key);

  @override
  State<ItemOverviewShare> createState() => _ItemOverviewShareState();
}

class _ItemOverviewShareState extends State<ItemOverviewShare> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.shoppingList.name),
      ),
      body: ItemTiles(widget.shoppingList, ComponentMode.share),
    );
  }
}
