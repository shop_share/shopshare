import 'package:flutter/material.dart';
import 'package:shop_share/tools/component_mode.dart';
import 'package:shop_share/tools/shoppinglist_tiles.dart';

import '../entities/shared_shopping_list.dart';
import '../services/firestore_service.dart';

/// ManageCreatedLists
/// 
/// Dialog where created lists may be edited
class ManageCreatedLists extends StatefulWidget {
  const ManageCreatedLists({Key? key}) : super(key: key);

  @override
  State<ManageCreatedLists> createState() => _ManageCreatedListsState();
}

class _ManageCreatedListsState extends State<ManageCreatedLists>
    with AutomaticKeepAliveClientMixin<ManageCreatedLists> {
  @override
  bool get wantKeepAlive => true;

  List<SharedShoppingList> shoppingLists = [];

  /// initState
  /// 
  /// Fetches all the created shopping lists
  @override
  void initState() {
    super.initState();

    FirestoreService.getCreatedShoppingLists().then((lists) => {
      setState(() {
        shoppingLists = lists;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Created and shared lists"),
      ),
      body: ShoppingListTiles(shoppingLists, ComponentMode.shareOwn),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     showDialog(
      //       context: context,
      //       builder: (BuildContext context) => const AddListDialog(),
      //     ).then((value) => setState(() {
      //       shoppingLists.add(value);
      //     }));
      //   },
      //   tooltip: 'Create new shopping list',
      //   child: const Icon(Icons.add),
      // ),
    );
  }
}
