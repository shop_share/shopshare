import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shop_share/main.dart';
import 'package:shop_share/authentication/account_registration.dart';
import 'package:shop_share/authentication/password_forget.dart';

/// AuthenticationOverview
///
/// Overview Class for providing authentication services: login, registration,
///   password reset
class AuthenticationOverview extends StatefulWidget {
  const AuthenticationOverview({Key? key}) : super(key: key);

  @override
  State<AuthenticationOverview> createState() => _AuthenticationOverviewState();
}

class _AuthenticationOverviewState extends State<AuthenticationOverview> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  /// login
  ///
  /// Logs user in or throws exception, when there is something wrong
  void _login() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text,
          password: passwordController.text);
      final user = FirebaseAuth.instance.currentUser;
      if (user != null && user.emailVerified) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute<void>(builder: (context) {
              return const MyApp();
            }));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("E-Mail is not verified!"),
          behavior: SnackBarBehavior.floating,
        ));
        FirebaseAuth.instance.signOut();
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'invalid-email') {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("E-mail address invalid!"),
          behavior: SnackBarBehavior.floating,
        ));
      }
      if (e.code == 'user-not-found') {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("User does not exist!"),
          behavior: SnackBarBehavior.floating,
        ));
      } else if (e.code == 'wrong-password') {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Wrong password!"),
          behavior: SnackBarBehavior.floating,
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Expanded(
              flex: 3,
              child: SizedBox(height: 60),
            ),
            Expanded(
              child: Image.asset('assets/shopshare_logo_zugeschnitten.jpg'),
              flex: 3,
            ),
            const Expanded(
              child: SizedBox(height: 60),
            ),
            Expanded(
              flex: 2,
              child: SizedBox(
                child: TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    labelText: 'E-mail address',
                    border: OutlineInputBorder(),
                  ),
                ),
                width: 300,
              ),
            ),
            //Expanded(
            //  child: SizedBox(height: 15)
            // ),
            Expanded(
                flex: 2,
                child: SizedBox(
                  child: TextFormField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      labelText: 'password',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  width: 300,
                )),
            const Expanded(
              child: SizedBox(height: 20),
            ),
            ElevatedButton(
              onPressed: () async {
                _login();
              },
              child: const Text('Login'),
            ),
            const Expanded(
              child: SizedBox(height: 20),
            ),
            ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute<void>(builder: (context) {
                    return const AccountRegistration();
                  }));
                },
                child: const Text("Register!")),
            const Expanded(
              child: SizedBox(height: 60),
            ),
            Expanded(
              child: TextButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute<void>(builder: (context) {
                    return const PasswordForget();
                  }));
                },
                child: const Text("Forgot your password?"),
              ),
            ),
            const Expanded(
              flex: 3,
              child: SizedBox(height: 60),
            ),
          ],
        ),
      ),
    );
  }
}
