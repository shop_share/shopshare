import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

/// PasswordForget
///
/// Dialog to reset ShopShare password
class PasswordForget extends StatefulWidget {
  const PasswordForget({Key? key}) : super(key: key);

  @override
  State<PasswordForget> createState() => _PasswordForgetState();
}

class _PasswordForgetState extends State<PasswordForget> {
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              "Reset Password",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            ),
            const SizedBox(height: 50),
            SizedBox(
              child: TextFormField(
                controller: emailController,
                decoration: const InputDecoration(
                  labelText: 'E-mail address',
                  border: OutlineInputBorder(),
                ),
              ),
              width: 200,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () async {
                try {
                  await FirebaseAuth.instance
                      .sendPasswordResetEmail(email: emailController.text);
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Confirmation mail was sent!"),
                    behavior: SnackBarBehavior.floating,
                  ));
                } on FirebaseAuthException catch (e) {
                  if (e.code == 'invalid-email') {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("E-mail address invalid!"),
                      behavior: SnackBarBehavior.floating,
                    ));
                  }
                  if (e.code == 'user-not-found') {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("User does not exist!"),
                      behavior: SnackBarBehavior.floating,
                    ));
                  }
                }
              },
              child: const Text('Reset'),
            ),
          ],
        ),
      ),
    );
  }
}
