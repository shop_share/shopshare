import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

/// AccountRegistration
///
/// Dialog to create new ShopShare account
class AccountRegistration extends StatefulWidget {
  const AccountRegistration({Key? key}) : super(key: key);

  @override
  State<AccountRegistration> createState() => _AccountRegistrationState();
}

class _AccountRegistrationState extends State<AccountRegistration> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordRepeatController = TextEditingController();

  /// register
  ///
  /// Sends registration mail for new users
  void _register() async {
    if (passwordController.text == passwordRepeatController.text) {
      try {
        await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: emailController.text,
          password: passwordController.text,
        );
        final user = FirebaseAuth.instance.currentUser;
        if (user != null) {
          await user.sendEmailVerification();
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("Confirmation mail was sent!"),
            behavior: SnackBarBehavior.floating,
          ));
        }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'weak-password') {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("Password too short! At least 6 symbols!"),
            behavior: SnackBarBehavior.floating,
          ));
        }
        if (e.code == 'invalid-email') {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("E-mail address invalid!"),
            behavior: SnackBarBehavior.floating,
          ));
        } else if (e.code == 'email-already-in-use') {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("User already exists!"),
            behavior: SnackBarBehavior.floating,
          ));
        }
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Passwords do not match!"),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "Register for ShopShare",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
          ),
          const SizedBox(height: 50),
          SizedBox(
            child: TextFormField(
              controller: emailController,
              decoration: const InputDecoration(
                labelText: 'E-mail address',
                border: OutlineInputBorder(),
              ),
            ),
            width: 300,
          ),
          const SizedBox(height: 15),
          SizedBox(
            child: TextFormField(
              obscureText: true,
              controller: passwordController,
              decoration: const InputDecoration(
                labelText: 'password',
                border: OutlineInputBorder(),
              ),
            ),
            width: 300,
          ),
          const SizedBox(height: 15),
          SizedBox(
            child: TextFormField(
              obscureText: true,
              controller: passwordRepeatController,
              decoration: const InputDecoration(
                labelText: 'Repeat password',
                border: OutlineInputBorder(),
              ),
            ),
            width: 300,
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: () {
              _register();
            },
            child: const Text('Register'),
          ),
        ],
      )),
    );
  }
}
