import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/services/sqlite_handler.dart';
import 'package:shop_share/tools/component_mode.dart';

import '../create/add_item_dialog.dart';
import '../entities/item.dart';

/// ItemTiles
///
/// Reusable widget for presenting items of a shopping list
class ItemTiles extends StatefulWidget {
  final ShoppingList shoppingList;
  final ComponentMode mode;

  const ItemTiles(this.shoppingList, this.mode, {Key? key}) : super(key: key);

  @override
  State<ItemTiles> createState() => _ItemTilesState();
}

class _ItemTilesState extends State<ItemTiles> {
  List<Item> openItems = []; //Item which have to be bought
  List<Item> closedItems = []; //Finished items

  var _currentTouchPosition;

  @override
  void initState() {
    super.initState();
    openItems = widget.shoppingList.items;
  }

  /// mapListToTile
  ///
  /// Draws widgets (=tiles) for every item in the list.
  /// @param items - the list of items
  /// @param completed - true, if the tile should be drawn as closed/finished
  /// @returns List of Containers/Tiles
  Iterable<Container> mapListToTile(List<Item> items, bool completed) =>
      items.map((item) {
        final widgetKey = GlobalKey();
        return Container(
          child: GestureDetector(
            onTapDown: (TapDownDetails details) {
              _currentTouchPosition = details.globalPosition;
            },
            child: ListTile(
              key: widgetKey,
              title: Center(
                  child: Column(
                    children: [
                      AutoSizeText(item.amount.toString(), maxLines: 1,maxFontSize: 20,
                      style: TextStyle(fontSize: 20),),
                      AutoSizeText(item.name, maxLines: 2,),
                      SizedBox(height: 10),
                      if (item.expiry != null) ...[
                        Text(item.expiry!.day.toString() + "." + item.expiry!.month.
                        toString() + "." + item.expiry!.year.toString(), style:
                        TextStyle(fontSize: 10, color: Colors.red),),
                      ],
                      if (item.note != "") ...[
                        Icon(Icons.comment, size: 15,)
                      ]
                    ],
                  )

              ),
              onTap: () {
                if (!completed) {
                  setState(() {
                    openItems.remove(item);
                    closedItems.add(item);
                  });
                } else {
                  setState(() {
                    closedItems.remove(item);
                    openItems.add(item);
                  });
                }
                /*
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const ItemView();
                })); zu item verwaltung -> dialog?*/
                /*Future<void>.delayed(
                  const Duration(),
                  () => showDialog(
                        context: context,
                        builder: (BuildContext context) => AddItemDialog(
                            title: "Edit Item",
                            itemName: item.name,
                            itemNote: item.note,
                            itemExpiry: item.expiry,
                            shoppingListId: item.shoppingListId,
                            mode: widget.mode),
                      ).then((value) => setState(() {
                            widget.shoppingList.items.remove(item);
                            widget.shoppingList.items.add(value);
                          }))); */
              },
              onLongPress: () {
                  final RenderBox overlay = Overlay.of(context)?.context.findRenderObject() as RenderBox;
                  showMenu(
                      context: context,
                      position: RelativeRect.fromRect(
                        _currentTouchPosition & Size(40, 40),
                        Offset.zero & overlay.size
                      ),
                      items: <PopupMenuEntry>[
                        PopupMenuItem(child: Text(item.amount.toString() + " " +
                          item.name)),
                        if (item.note != "") ...[
                          PopupMenuItem(child: Text("Note: " + item.note,
                          style: TextStyle(color: Colors.red),)),
                        ],
                        if (widget.mode == ComponentMode.create) ...[
                          const PopupMenuDivider(),
                          PopupMenuItem(
                              child: Row(
                                children: const [Text("Edit item")],
                              ),
                              onTap: () {
                                Future<void>.delayed(
                                    const Duration(),
                                        () => showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          AddItemDialog(
                                            title: "Edit Item",
                                            itemName: item.name,
                                            id: item.id,
                                            amount: item.amount.toString(),
                                            itemNote: item.note,
                                            itemExpiry: item.expiry,
                                            shoppingListId: item.shoppingListId,
                                            mode: widget.mode,
                                          ),
                                    ).then((value) => setState(() {
                                      widget.shoppingList.items
                                          .remove(item);
                                      widget.shoppingList.items.add(value);
                                      SqliteHandler.updateItem(value);
                                    })));
                              }),
                          PopupMenuItem(
                              child: Row(
                                children: const [Text("Delete item")],
                              ),
                              onTap: () {
                                setState(() {
                                  SqliteHandler.deleteItem(item.id);
                                  items.remove(item);
                                });
                              }),
                        ]
                      ]);
              },
              // subtitle: Text(item.expiry.day.toString() + "." + item.expiry.month.
              //     toString() + "." + item.expiry.year.toString()),
            ),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(5),
          color: completed ? Colors.green[100] : Colors.orange,
        );
      });

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          body: Column(
        children: [
          TabBar(tabs: [
            Tab(
              text: "Open",
            ),
            Tab(text: "Closed")
          ], labelColor: Colors.black),
          Expanded(
            child: TabBarView(
              children: [
                Scaffold(
                  body: SingleChildScrollView(
                    child: GridView.count(
                        mainAxisSpacing: 20,
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        crossAxisSpacing: 20,
                        scrollDirection: Axis.vertical,
                        crossAxisCount: 3,
                        children: mapListToTile(openItems, false).toList()),
                  ),
                ),
                Scaffold(
                  body: SingleChildScrollView(
                    child: GridView.count(
                        mainAxisSpacing: 20,
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        crossAxisSpacing: 20,
                        scrollDirection: Axis.vertical,
                        crossAxisCount: 3,
                        children: mapListToTile(closedItems, true).toList()),
                  ),
                ),
              ],
            ),
          )
        ],
      )),
      // GridView.count(
      //     mainAxisSpacing: 20,
      //     shrinkWrap: true,
      //     physics: ScrollPhysics(),
      //     crossAxisSpacing: 20,
      //     crossAxisCount: 3,
      //     c
    );
  }
}
