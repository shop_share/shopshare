import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../services/firestore_service.dart';

/// Chat
///
/// Widget for providing chat functionality for questions about the shared shopping list
class Chat extends StatefulWidget {
  final String shoppingListId;

  const Chat(this.shoppingListId, {Key? key}) : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
      .collection('Chat').orderBy("time")
      .snapshots(); //Build stream of chat messages from firebase ordered by their timestamps
  final chatController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Chat"),
        ),
        body: Stack(children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            stream: _usersStream,
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {

              if (snapshot.hasError) {
                return const Text('Something went wrong');
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Text("Loading");
              }

              return ListView(
                children: snapshot.data!.docs.where((element) => element["shoppingListId"] == widget.shoppingListId)
                    .map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;
                      print(data["time"]);
                      return ListTile(
                        title: Text(data['message']),
                        tileColor: data['user'] == FirestoreService.userID
                            ? Colors.lightGreen.shade200
                            : Colors.grey.shade200,
                      );
                    })
                    .toList()
                    .cast(),
              );
            },
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 10, bottom: 10, top: 10),
              height: 60,
              width: double.infinity,
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: chatController,
                      decoration: const InputDecoration(
                          hintText: "Write message...",
                          hintStyle: TextStyle(color: Colors.black54),
                          border: InputBorder.none),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  FloatingActionButton(
                    onPressed: () async {
                      await FirestoreService.sendMessage(
                          widget.shoppingListId, chatController.text);
                      chatController.text = "";
                    },
                    child: const Icon(
                      Icons.send,
                      color: Colors.white,
                      size: 18,
                    ),
                    backgroundColor: Colors.blue,
                    elevation: 0,
                  ),
                ],
              ),
              margin: const EdgeInsets.all(5),
            ),
          ),
        ]));
  }
}
