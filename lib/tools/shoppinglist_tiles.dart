import 'package:flutter/material.dart';
import 'package:shop_share/create/share_list_dialog.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/services/firestore_service.dart';
import 'package:shop_share/share/item_overview_share.dart';
import 'package:shop_share/tools/chat.dart';
import 'package:shop_share/tools/component_mode.dart';

import '../create/edit_list_dialog.dart';
import '../create/item_overview_create.dart';
import '../entities/shared_shopping_list.dart';
import '../services/sqlite_handler.dart';

/// ShoppingListTiles
/// 
/// Reusable widget to draw lists of shopping lists. Used for example in CreateOverview and 
class ShoppingListTiles extends StatefulWidget {
  final List<ShoppingList> shoppingLists;
  final ComponentMode mode;

  const ShoppingListTiles(this.shoppingLists, this.mode, {Key? key})
      : super(key: key);

  @override
  State<ShoppingListTiles> createState() => _ShoppingListTilesState();
}

class _ShoppingListTilesState extends State<ShoppingListTiles> {
  List<GlobalKey> keys = [];

  RelativeRect _getRelativeRect(GlobalKey key) {
    return RelativeRect.fromSize(
        _getWidgetGlobalRect(key), const Size(200, 200));
  }

  Rect _getWidgetGlobalRect(GlobalKey key) {
    final RenderBox renderBox =
        key.currentContext!.findRenderObject() as RenderBox;
    var offset = renderBox.localToGlobal(Offset.zero);
    debugPrint('Widget position: ${offset.dx} ${offset.dy}');
    return Rect.fromLTWH(offset.dx / 3.1, offset.dy * 1.05,
        renderBox.size.width, renderBox.size.height);
  }

  /// mapListToTile
  ///
  /// Draws widgets (=tiles) for every shopping list in the list.
  /// @param shoppingListsTemp - the list of shopping lists
  /// @returns List of Containers/Tiles
  Iterable<Container> mapListToTile(List<ShoppingList> shoppingListsTemp) =>
      shoppingListsTemp.map((shoppingList) {
        String name = shoppingList.name;
        final widgetKey = GlobalKey();
        return Container(
          child: GestureDetector(
            key: widgetKey,
            onLongPress: () {
              switch (widget.mode) {
                case ComponentMode.create:
                  pressLongCreateSection(shoppingList, widgetKey);
                  break;
                case ComponentMode.share:
                  pressLongCreateSectionShare(shoppingList, widgetKey);
                  break;
                case ComponentMode.shareOwn:
                  pressLongCreateSectionShareOwn(shoppingList, widgetKey);
                  break;
              }
            },
            child: widget.mode == ComponentMode.create
                ? ListTile(
                    title: Text(shoppingList.name),
                    onTap: () async {
                      shoppingList.items =
                          await SqliteHandler.getItems(shoppingList.id);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return ItemOverviewCreate(shoppingList);
                      }));
                    },
                    trailing: IconButton(
                      icon: const Icon(Icons.share),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              ShareListDialog(shoppingList),
                        ).then((newList) async {
                          await FirestoreService.addShoppingList(newList);
                          setState(() {
                            SqliteHandler.deleteShoppingList(shoppingList.id);
                            widget.shoppingLists.remove(shoppingList);
                          });
                          print("share complete");
                        });
                      },
                    ),
                  )
                : ListTile(
                    leading: widget.mode == ComponentMode.shareOwn &&
                        (shoppingList as SharedShoppingList).assigned != null
                        ? const Icon(Icons.bookmark)
                        : null,
                    title: Text(shoppingList.name),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return ItemOverviewShare(shoppingList);
                      }));
                    },
                    trailing: (shoppingList as SharedShoppingList).assigned != null ? IconButton(
                      icon: const Icon(Icons.chat),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Chat(shoppingList.id);
                        }));
                      },
                    ) : null),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(5),
          color: Colors.green[100],
        );
      });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: mapListToTile(widget.shoppingLists).toList())
    );
  }

  /// pressLongCreateSection
  ///
  /// Create popup menu when long pressed on one shopping list
  /// @param shoppingList - Shopping list on which the long press relates to
  /// @param widgetKey - Flutter needed global widget key
  void pressLongCreateSection(ShoppingList shoppingList, GlobalKey widgetKey) {
    showMenu(
      items: <PopupMenuEntry>[
        PopupMenuItem(
          child: Row(
            children: const [Text("Edit name")],
          ),
          onTap: () {
            Future<void>.delayed(
                const Duration(),
                () => showDialog(
                      context: context,
                      builder: (BuildContext context) => EditListDialog(
                        shoppingList: shoppingList,
                      ),
                    ).then((newShoppingList) => setState(() {
                          widget.shoppingLists.remove(shoppingList);
                          widget.shoppingLists.add(newShoppingList);
                          SqliteHandler.updateShoppingList(newShoppingList);
                        })));
          },
        ),
        PopupMenuItem(
            child: Row(
              children: const [Text("Delete shopping list")],
            ),
            onTap: () {
              setState(() {
                widget.shoppingLists.remove(shoppingList);
                SqliteHandler.deleteShoppingList(shoppingList.id);
              });
            }),
      ],
      context: context,
      position: _getRelativeRect(widgetKey),
    );
  }

  /// pressLongCreateSectionShare
  ///
  /// Create popup menu when long pressed on one shopping list in share component
  /// @param shoppingList - Shopping list on which the long press relates to
  /// @param widgetKey - Flutter needed global widget key
  void pressLongCreateSectionShare(ShoppingList shoppingList, GlobalKey widgetKey) {
    SharedShoppingList sharedShoppingList = shoppingList as SharedShoppingList;
    showMenu(
      items: <PopupMenuEntry>[
        PopupMenuItem(
          child: Row(
            children: const [Text("Unassign list")],
          ),
          onTap: () {
            FirestoreService.unassignShoppingList(sharedShoppingList);
            setState(() {
              widget.shoppingLists.remove(shoppingList);
            });
          },
        )
      ],
      context: context,
      position: _getRelativeRect(widgetKey),
    );
  }

  /// pressLongCreateSectionShareOwn
  ///
  /// Create popup menu when long pressed on one shopping list in share component in ManageCreatedLists. 
  /// @param shoppingList - Shopping list on which the long press relates to
  /// @param widgetKey - Flutter needed global widget key
  void pressLongCreateSectionShareOwn(ShoppingList shoppingList, GlobalKey widgetKey) {
    SharedShoppingList sharedShoppingList = shoppingList as SharedShoppingList;
    showMenu(
      items: <PopupMenuEntry>[
        PopupMenuItem(
          child: Row(
            children: const [Text("Delete list")],
          ),
          onTap: () {
            if (sharedShoppingList.assigned == null) {
              SqliteHandler.getUnassignedConstant().then((value) {
                SqliteHandler.updateUnassignedConstant(value - 1).then((value) {
                  FirestoreService.removeShoppingList(sharedShoppingList);
                  setState(() {
                    widget.shoppingLists.remove(shoppingList);
                  });
                });
              });

            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text('Cannot delete this list. It is assigned to someone!')
                  )
              );
            }

          },
        )
      ],
      context: context,
      position: _getRelativeRect(widgetKey),
    );
  }
}
