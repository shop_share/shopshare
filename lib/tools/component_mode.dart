///Enum to differ between call from create or share components at ItemTiles or ShoppingListTiles
enum ComponentMode{
  create,
  share,
  shareOwn
}
