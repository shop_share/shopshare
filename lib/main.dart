import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shop_share/authentication/authentication_overview.dart';
import 'package:shop_share/settings.dart';

import 'services/background_service.dart';
import 'create/create_overview.dart';
import 'firebase_options.dart';
import 'services/firestore_service.dart';
import 'share/share_overview.dart';

import 'dart:async';

/// Main
///
/// Entry point for application.
/// Initializes background service, firebase connection and starts app.

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //Init firebase
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  //Init background service
  initBackgroundService();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
        title: 'ShopShare',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const DefaultTabController(length: 2, child: MyHomePage()));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();

    //Check if user is signed in. If not, send them back to authentication screen.
    //Else: collect user id for db operations
    Future(() {
      FirebaseAuth.instance.userChanges().listen((user) {
        if (user == null) {
          Navigator.of(context)
              .pushReplacement(MaterialPageRoute<void>(builder: (context) {
            return const AuthenticationOverview();
          }));
        } else {
          FirestoreService.userID = user.uid;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ShopShare'),
        actions: [
          IconButton(
              onPressed: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const Settings();
                }));
              },
              icon: const Icon(Icons.settings))
        ],
        bottom: const TabBar(
            indicatorColor: Colors.white,
            isScrollable: false,
            tabs: [
              Tab(text: 'Create', icon: Icon(Icons.add_shopping_cart)),
              Tab(text: 'Share', icon: Icon(Icons.share))
            ]),
      ),
      body: const TabBarView(children: [CreateOverview(), ShareOverview()]),
    );
  }
}
