import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:shop_share/services/firestore_service.dart';
import 'package:sqflite/sqflite.dart';

import '../entities/item.dart';
import '../entities/shopping_list.dart';

// SqliteHandler
//
// This class handles the storage of local shopping lists and items via
// sqlite.
class SqliteHandler {
  late Future<Database> database;

  // openConnection
  //
  // Connects to the local sqlite file on the users phone.
  // Tables handled by the service:
  // - ShoppingLists: what do you think?
  // - Items: pretty obvious don't you think
  // - Constants: caching for the background service, handling push notifications
  static Future<Database> openConnection() async {
    WidgetsFlutterBinding.ensureInitialized();
    return openDatabase(
      join(await getDatabasesPath(), 'shopshare_database.db'),
    );
  }

  // createTables
  //  
  // Creates the tables for shopping lists, items and constants
  static void createTables() async {
    var database = await openConnection();
    String createShoppingListTable =
        'CREATE TABLE IF NOT EXISTS shoppingList(id TEXT PRIMARY KEY, name TEXT, creatorId TEXT)';
    String createItemTable =
        'CREATE TABLE IF NOT EXISTS item(id TEXT PRIMARY KEY, name TEXT, amount INTEGER, note TEXT, expiry TEXT, shoppingListId TEXT)';
    String createConstantsTable =
        'CREATE TABLE IF NOT EXISTS constants(id TEXT PRIMARY KEY, count INTEGER)';
    await database.execute(createShoppingListTable);
    await database.execute(createItemTable);
    await database.execute(createConstantsTable);
  }

  // insertShoppingList
  //
  // Adds a shopping list to the database or replaces an existing one if already present in the database
  // @param: shoppingList - the shopping list to the added
  static Future<void> insertShoppingList(ShoppingList shoppingList) async {
    var database = openConnection();
    final db = await database;
    await db.insert(
      'shoppingList',
      shoppingList.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // insertItem
  //
  // Adds an item to the database or replaces an existing one if already present in the database
  // @param: item - the item to the added
  static Future<void> insertItem(Item item) async {
    var database = openConnection();
    final db = await database;
    await db.insert(
      'item',
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // getShoppingLists
  //
  // Retrieves all shopping lists from the database
  // @return: list of shoppingList objects
  static Future<List<ShoppingList>> getShoppingLists() async {
    var database = openConnection();
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('shoppingList');
    return List.generate(maps.length, (i) {
      var list =  ShoppingList.withId(
          (maps[i]['name']), (maps[i]['id']), (maps[i]['creatorId']));
      return list;
    });
  }

  // getItems
  //
  // Retrieves all items from the database for one shopping list
  // @param: shoppingListId - id of shopping list whose items are wanted
  // @return: list of item objects
  static Future<List<Item>> getItems(String shoppingListId) async {
    var database = openConnection();
    final db = await database;
    final List<Map<String, dynamic>> maps =
        await db.query('item', where: 'shoppingListId = ?', whereArgs: [shoppingListId]);
    return List.generate(maps.length, (i) {
      var expiry = maps[i]['expiry'];
      return Item(
        name: (maps[i]['name']),
        amount: (maps[i]['amount'] ?? 1),
        id: (maps[i]['id']),
        expiry: (expiry != null && expiry != "null") ? DateTime.parse(expiry) : null,
        note: (maps[i]['note']),
        shoppingListId: (maps[i]['shoppingListId']),
      );
    });
  }


  // deleteShoppingList
  //
  // Deletes a specific shopping list from the database
  // @param: id - id of shopping list which will be deleted
  static Future<void> deleteShoppingList(String id) async {
    var database = openConnection();
    final db = await database;
    await db.delete(
      'shoppingList',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  // deleteItem
  //
  // Deletes a specific item of a shopping list from the database
  // @param: id - id of item which will be deleted
  static Future<void> deleteItem(String id) async {
    var database = openConnection();
    final db = await database;
    await db.delete(
      'item',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  // updateShoppingList
  //
  // Updates the attributes of a shopping list
  // @param: shoppingList - shoppingList to be updated
  static Future<void> updateShoppingList(ShoppingList shoppingList) async {
    var database = openConnection();
    final db = await database;
    await db.update(
      'shoppingList',
      shoppingList.toMap(),
      where: 'id = ?',
      whereArgs: [shoppingList.id],
    );
  }

  // updateItem
  //
  // Updates the attributes of an item
  // @param: item - item to be updated
  static Future<void> updateItem(Item item) async {
    var database = openConnection();
    final db = await database;
    await db.update(
      'item',
      item.toMap(),
      where: 'id = ?',
      whereArgs: [item.id],
    );
  }

  // updateUnassignedConstant
  //
  // Updates the count of shopping lists which are public but not yet assigned
  // @param: newCount - new count of shopping lists
  static Future<void> updateUnassignedConstant(int newCount) async {
    var database = openConnection();
    final db = await database;
    await db.execute(""
        "INSERT INTO constants(id, count) VALUES ('unassigned_count', ?) "
        "ON CONFLICT(id) DO UPDATE SET count=?;", [newCount, newCount]);
  }

  // deleteChatCounts
  //
  // Deletes all meta data for the chat when users logs out
  static Future<void> deleteChatCounts() async {
    var db = await openConnection();
    await db.delete("constants",
      where: "id LIKE 'chatcount_%'");
  }

  // countChatMessages
  //
  // Counts number of messages the other person has sent and writes them to the db
  // @return: 
  static Future<void> countChatMessages() async {
    var sharedShoppingLists = await FirestoreService.getCreatedShoppingLists();
    var db = await openConnection();
    for (var list in sharedShoppingLists) {
      if (list.assigned == null) continue;
      var messageCount = await FirestoreService
          .getMessageCountFromOtherFromShoppingList(list.id);
      db.execute(
        "INSERT INTO constants(id, count) VALUES ('chatcount_${list.id}', ?) "
            "ON CONFLICT(id) DO UPDATE SET count=?", [messageCount, messageCount]);
    }
  }

  // updateChatMessages
  //
  // Compares FireStore chat messages and local chat messages to check if push 
  // notification needs to be sent
  // @return: all names of shopping lists, where a message was sent by the other person
  static Future<List<String>> updateChatMessages() async {
    var sharedShoppingLists = await FirestoreService.getCreatedShoppingLists();
    sharedShoppingLists.addAll(await FirestoreService.getAssignedShoppingLists());
    var db = await openConnection();
    List<String> listNames = [];
    for (var list in sharedShoppingLists) {
      if (list.assigned == null) continue;
      var messageCount = await FirestoreService
          .getMessageCountFromOtherFromShoppingList(list.id);
      var localCount = await db.query("constants", where: "id='chatcount_${list.id}'");
      if (localCount.isNotEmpty) {
        var localCountNumber = localCount[0]["count"] as int;
        if (messageCount > localCountNumber) {
          if (list.creator == FirestoreService.userID) {
            listNames.add("created list " + list.name);
          } else {
            listNames.add("assigned list " + list.name);
          }
        }
      }
      db.execute(
          "INSERT INTO constants(id, count) VALUES ('chatcount_${list.id}', ?) "
              "ON CONFLICT(id) DO UPDATE SET count=?", [messageCount, messageCount]);
    }
    return listNames;
  }

  // getUnassignedConstant
  //
  // Retrieves nunber of shopping lists which are shared but not assigned
  // @return: count of those lists
  static Future<int> getUnassignedConstant() async {
    var database = openConnection();
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('constants');
    try {
      return maps[0]['count'];
    } catch (e) {
      return 0;
    }
  }

  // deleteAllLocalData
  //
  // Deletes all data from the sqlite database
  static Future<void> deleteAllLocalData() async {
    var database = await openConnection();

    await database.execute("DROP TABLE IF EXISTS item");
    await database.execute("DROP TABLE IF EXISTS shoppingList");
    await database.execute("DROP TABLE IF EXISTS constants");
  }
}
