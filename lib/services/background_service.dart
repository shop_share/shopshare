import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shop_share/services/sqlite_handler.dart';

import '../firebase_options.dart';
import 'firestore_service.dart';

import 'dart:async';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';

/// background_service
///
/// code for running the background service to create push notifications

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  void initBackgroundService() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings("logo");
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    await initializeService();
  }


  void selectNotification(String? payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future<void> initializeService() async {
    final service = FlutterBackgroundService();
    await service.configure(
      androidConfiguration: AndroidConfiguration(
        // this will executed when app is in foreground or background in separated isolate
        onStart: onStart,

        // auto start service
        autoStart: true,
        isForegroundMode: false,
      ),
      iosConfiguration: IosConfiguration(
        // auto start service
        autoStart: true,

        // this will executed when app is in foreground in separated isolate
        onForeground: onStart,

        // you have to enable background fetch capability on xcode project
        onBackground: onIosBackground,
      ),
    );
    service.startService();
  }

  bool onIosBackground(ServiceInstance service) {
    WidgetsFlutterBinding.ensureInitialized();
    print('FLUTTER BACKGROUND FETCH');

    return true;
  }

  // Contains main logic for background service
  void onStart(ServiceInstance service) async {

    if (service is AndroidServiceInstance) {
      service.on('setAsForeground').listen((event) {
        service.setAsForegroundService();
      });

      service.on('setAsBackground').listen((event) {
        service.setAsBackgroundService();
      });
    }

    service.on('stopService').listen((event) {
      service.stopSelf();
    });

    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    SqliteHandler.createTables();
    bool chatCounted = false;

    // If user is logged in, init constants table
    // If user logged out, delete cached data
    FirebaseAuth.instance.userChanges().listen((user) {
      if (user == null) {
        FirestoreService.userID = "";
        SqliteHandler.updateUnassignedConstant(0);
        SqliteHandler.deleteChatCounts();
      } else {
        FirestoreService.userID = user.uid;
        SqliteHandler.countChatMessages().then((value) {
          chatCounted = true;
        });
      }
    });

    // Repeat check for changes all 5 seconds
    Timer.periodic(const Duration(seconds: 5), (timer) async {
      final hello = "world";
      print(hello);

      print(FirestoreService.userID);

      if (service is AndroidServiceInstance && FirestoreService.userID != "") {

        // Count unassigned lists to check if one has assigned a list

        var onlineCount = await FirestoreService.countUnassignedLists();
        var localCount = await SqliteHandler.getUnassignedConstant();

        if (onlineCount < localCount) {
          const AndroidNotificationDetails androidPlatformChannelSpecifics =
          AndroidNotificationDetails('your channel id', 'your channel name',
              channelDescription: 'your channel description',
              importance: Importance.max,
              priority: Priority.high,
              ticker: 'ticker');
          const NotificationDetails platformChannelSpecifics =
          NotificationDetails(android: androidPlatformChannelSpecifics);
          await flutterLocalNotificationsPlugin.show(
              0, 'A shopping list was assigned!',
              'Someone assigned one of your shared shopping lists!',
              platformChannelSpecifics,
              payload: 'item x');
          await SqliteHandler.updateUnassignedConstant(onlineCount);
        } else if (onlineCount > localCount) {
          await SqliteHandler.updateUnassignedConstant(onlineCount);
        }


        // Count all chat messages from all created/assigned lists to check for new messages

        if (chatCounted) {
          SqliteHandler.updateChatMessages().then((listNames) async {
            for (var name in listNames) {
              const AndroidNotificationDetails androidPlatformChannelSpecifics =
              AndroidNotificationDetails('your channel id', 'your channel name',
                  channelDescription: 'your channel description',
                  importance: Importance.max,
                  priority: Priority.high,
                  ticker: 'ticker');
              const NotificationDetails platformChannelSpecifics =
              NotificationDetails(android: androidPlatformChannelSpecifics);
              await flutterLocalNotificationsPlugin.show(
                  0, 'New chat message!',
                  'A new message in your $name',
                  platformChannelSpecifics,
                  payload: 'item x');
            }
          });
        }

      }

      /// you can see this log in logcat
      print('FLUTTER BACKGROUND SERVICE: ${DateTime.now()}');

      // test using external plugin
      final deviceInfo = DeviceInfoPlugin();
      String? device;
      if (Platform.isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        device = androidInfo.model;
      }

      if (Platform.isIOS) {
        final iosInfo = await deviceInfo.iosInfo;
        device = iosInfo.model;
      }

      service.invoke(
        'update',
        {
          "current_date": DateTime.now().toIso8601String(),
          "device": device,
        },
      );
    });
  }
