import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shop_share/entities/shared_shopping_list.dart';

import '../entities/item.dart';

// FireStoreFactory
//
// Handles the casting of firestore data to flutter objects
class FirestoreFactory {

  // rawToSharedShoppingList
  //
  // Casts firestore data into a SharedShoppingList object
  // @param: element - shopping list data
  // @param: items - items of shopping list
  // @return: SharedShoppingList object
  static SharedShoppingList rawToSharedShoppingList(DocumentSnapshot element, List<Item> items) {
    var shoppingList = SharedShoppingList(
        name: element.get('name'), expiry: element.get('expiry').toDate());
    shoppingList.creator = element.get('creator');
    shoppingList.id = element.id;
    shoppingList.location = element.get('geopoint')['geopoint'];
    shoppingList.items = items;
    shoppingList.assigned = element.get('assigned');
    return shoppingList;
  }

  // rawToItem
  //
  // Casts firestore data into an item object
  // @param: element - item data
  // @return: Item object
  static Item rawToItem(DocumentSnapshot element) {
    var amount = element.get('amount');
    var expiry = element.get('expiry');
    return Item(
        name: element.get('name'),
        expiry: expiry?.toDate(),
        note: element.get('note'),
        amount: amount ?? 1,
        shoppingListId: element.get('shoppingListId'));
  }
}
