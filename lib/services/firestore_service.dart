import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shop_share/entities/item.dart';
import 'package:shop_share/entities/shared_shopping_list.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:shop_share/services/firestore_factory.dart';

/// FireStoreService
/// 
/// Class handling all things FireStore and its connection
class FirestoreService {
  static Geoflutterfire geo = Geoflutterfire();
  static CollectionReference items =
      FirebaseFirestore.instance.collection('Items');
  static CollectionReference chat =
      FirebaseFirestore.instance.collection('Chat');
  static CollectionReference<Map<String, dynamic>> shoppingLists =
      FirebaseFirestore.instance.collection('ShoppingList');

  static var userID = "";

  /// getShoppingListsAtCoordinate
  /// 
  /// Fetches all shopping lists for a given set of coordinates
  /// @param: position - location the lists will be fetched for
  /// @return: List of shopping lists available at the given location
  static Future<List<SharedShoppingList>> getShoppingListsAtCoordinate(
      LatLng position) async {
    List<SharedShoppingList> slists = [];
    GeoFirePoint center =
        geo.point(latitude: position.latitude, longitude: position.longitude);
    Stream<List<DocumentSnapshot>> stream = geo
        .collection(collectionRef: shoppingLists)
        .within(center: center, radius: 15, field: 'geopoint');
    await stream.first.then((list) async => {
          for (var element in list)
            {
              if (element.get('creator') != userID && element.get('assigned') == null)
                {
                  slists.add(FirestoreFactory.rawToSharedShoppingList(
                      element,
                      await FirestoreService.getAllItemsOfShoppingList(
                          element.id)))
                }
            }
        });
    return slists;
  }

  /// assignNewShoppingList
  /// 
  /// Assigns a shopping list to the user
  /// @param: shoppingList - List that will be assigned
  static void assignNewShoppingList(SharedShoppingList shoppingList) async {
    shoppingList.assigned = userID;
    await shoppingLists.doc(shoppingList.id).update({'assigned': userID});
  }

  /// unassignNewShoppingList
  /// 
  /// Removes a shopping list from the assigned lists of the user
  /// @param: shoppingList - List that will be unassigned
  static void unassignShoppingList(SharedShoppingList shoppingList) async {
    shoppingList.assigned = null;
    await shoppingLists.doc(shoppingList.id).update({'assigned': null});
  }

  /// getAssignedShoppingLists
  /// 
  /// Fetches all shopping lists assigned to the user
  /// @return: List of assigned shopping lists
  static Future<List<SharedShoppingList>> getAssignedShoppingLists() async {
    List<SharedShoppingList> resList = [];
    var assignedLists = await shoppingLists
        .where("assigned", isEqualTo: FirestoreService.userID)
        .get();
    for (var element in assignedLists.docs) {
      List<Item> items =
          await FirestoreService.getAllItemsOfShoppingList(element.id);
      resList.add(FirestoreFactory.rawToSharedShoppingList(element, items));
    }
    return resList;
  }

  /// getCreatedShoppingLists
  /// 
  /// Fetches all shopping lists that were created by the user
  /// @return: List of created shopping lists
  static Future<List<SharedShoppingList>> getCreatedShoppingLists() async {
    List<SharedShoppingList> resList = [];
    var assignedLists = await shoppingLists
        .where("creator", isEqualTo: FirestoreService.userID)
        .get();
    for (var element in assignedLists.docs) {
      List<Item> items =
      await FirestoreService.getAllItemsOfShoppingList(element.id);
      resList.add(FirestoreFactory.rawToSharedShoppingList(element, items));
    }
    return resList;
  }

  /// addItem
  /// 
  /// Adds an item to a shopping list
  /// @param: item - Item that will be added to a given shopping list
  static Future addItem(Item item) {
    return items
        .doc(item.id)
        .set({
          'name': item.name,
          'note': item.note,
          'amount': item.amount,
          'expiry': item.expiry,
          'shoppingListId': item.shoppingListId
        })
        .then((value) => print("item added"))
        .catchError((error) => print("Failed to add item: $error"));
  }

  /// getMessageCountFromOtherFromShoppingList
  /// 
  /// Counts the amount of chat messages (local)
  /// @param: id - ID of a given shopping list
  /// @return Amount of chat messages for the given shopping list
  static Future<int> getMessageCountFromOtherFromShoppingList(String id) async {
    var messages = await chat
        .where("shoppingListId", isEqualTo: id)
        .get();
    int count = 0;
    for (var element in messages.docs) {
      if (element.get("user") != userID) {
        count++;
      }
    }
    return count;
  }

  /// sendMessage
  /// 
  /// Sends a message
  /// @param: shoppingListId - ID of a given shopping list
  /// @param: message - String containing the chat message
  /// @return The entire chat containing the given chat message
  static Future sendMessage(
      String shoppingListId, String message) {
    return chat
        .add({
          'user': FirestoreService.userID,
          'shoppingListId': shoppingListId,
          'message': message,
          'time': DateTime.now()
        })
        .then((value) => print("message added"))
        .catchError((error) => print("Failed to add message: $error"));
  }

  /// addShoppingList
  /// 
  /// Adds a shopping list to the local database
  /// @param: shoppingList - The new shopping list
  static Future addShoppingList(SharedShoppingList shoppingList) async {
    return shoppingLists
        .doc(shoppingList.id)
        .set({
          'name': shoppingList.name,
          'creator': userID,
          'expiry': shoppingList.expiry,
          'geopoint': geo
              .point(
                  latitude: shoppingList.location.latitude,
                  longitude: shoppingList.location.longitude)
              .data,
          'assigned': null
        })
        .then((value) => {
              for (var element in shoppingList.items) {addItem(element)},
              print("shoppingList added")
            })
        .catchError((error) => print("Failed to add shoppingList: $error"));
  }

  /// removeShoppingList
  /// 
  /// Removes a shopping list to the local database
  /// @param: shoppingList - An existing shopping list
  static Future removeShoppingList(SharedShoppingList shoppingList) async {
    return shoppingLists
        .doc(shoppingList.id)
        .delete();
  }

  /// getAllItemsOfShoppingList
  /// 
  /// Fetches all items from a given shopping list
  /// @param: shoppingListId - ID of an existing shopping list
  /// @return List of items held by the selected shopping list
  static Future<List<Item>> getAllItemsOfShoppingList(
      String shoppingListId) async {
    List<Item> resItems = [];
    var itemsOfList =
        items.where("shoppingListId", isEqualTo: shoppingListId).get();
    await itemsOfList.asStream().first.then((list) => {
          for (var element in list.docs)
            {resItems.add(FirestoreFactory.rawToItem(element))}
        });
    return resItems;
  }

  /// getSingleItem
  /// 
  /// Fetches a shopping item corresponding to a selected ID
  /// @param: itemId - unique identifier of a shopping item
  /// @return The item identified by the given ID
  static Future<Item> getSingleItem(String itemId) async {
    var itemDoc = await items.doc(itemId).get();
    return FirestoreFactory.rawToItem(itemDoc);
  }

  /// countUnassignedLists
  /// 
  /// Counts the amount of lists that are currently unassigned
  /// @return Amount of unassigned lists
  static Future<int> countUnassignedLists() async {
    int counter = 0;
    List<SharedShoppingList> resList = [];
    var assignedLists = await shoppingLists
        .where("creator", isEqualTo: userID)
        .get();
    for (var element in assignedLists.docs) {
      resList.add(FirestoreFactory.rawToSharedShoppingList(element, []));
    }
    for (var element in resList) {
      if (element.assigned == null) {
        counter++;
      }
    }
    return counter;
  }
}
