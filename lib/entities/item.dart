import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class Item {
  late String id;
  String name;
  int amount;
  DateTime? expiry;
  String note;
  String shoppingListId;

  Item(
      {id,
      required this.name,
      required this.expiry,
      required this.note,
        required this.amount,
      required this.shoppingListId}) {
    if (id == null) {
      this.id = const Uuid().v1();
    } else {
      this.id = id;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'amount': amount,
      'expiry': expiry.toString(),
      'note': note,
      'shoppingListId': shoppingListId
    };
  }
}
