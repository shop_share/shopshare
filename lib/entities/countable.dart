import 'item.dart';

class Countable extends Item {
  int count;

  Countable(
      {required String name,
      required DateTime expiry,
      required String note,
      required String shoppingListId,
      required int amount,
      required this.count})
      : super(
            name: name,
            expiry: expiry,
            note: note,
            shoppingListId: shoppingListId,
            amount: amount);
}
