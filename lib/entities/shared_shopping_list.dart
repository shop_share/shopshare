import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shop_share/entities/shopping_list.dart';
import 'package:shop_share/entities/state.dart';

class SharedShoppingList extends ShoppingList {
  late String? assigned;
  late DateTime expiry;

  late GeoPoint location;
  late State state;

  SharedShoppingList({required String name, required this.expiry})
      : super(name) {
    state = State.open;
    assigned = null;
  }
}
