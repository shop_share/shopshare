import 'item.dart';

class Liquid extends Item {
  double volume;

  Liquid(
      {required String name,
      required DateTime expiry,
      required String note,
      required String shoppingListId,
      required int amount,
      required this.volume})
      : super(
            name: name,
            expiry: expiry,
            note: note,
            shoppingListId: shoppingListId,
            amount: amount);
}
