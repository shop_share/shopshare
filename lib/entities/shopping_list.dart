import 'package:uuid/uuid.dart';

import 'item.dart';

class ShoppingList {
  late List<Item> items;
  late String id;
  String name;
  late String creator; //TODO: integrate
  late List<String> contributors; //TODO: integrate

  ShoppingList(this.name) {
    id = const Uuid().v1();
    items = [];
    contributors = [];
    creator = "testCreator";
  }

  ShoppingList.withId(this.name, this.id, this.creator) {
    items = [];
    contributors = [];
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id.toString(),
      'name': name,
      'creatorId': creator,
    };
  }
}
