import 'item.dart';

class Weighable extends Item {
  double weight;

  Weighable(
      {required String name,
      required DateTime expiry,
      required String note,
      required String shoppingListId,
      required int amount,
      required this.weight})
      : super(
            name: name,
            expiry: expiry,
            note: note,
            shoppingListId: shoppingListId,
            amount: amount);
}
