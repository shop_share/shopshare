import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shop_share/services/sqlite_handler.dart';

/// Settings
///
/// Settings widget containing options to log out and delete local data
class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings"),
      ),
      body: Column(
        children: [
          Image.asset('assets/shopshare_logo_zugeschnitten.jpg'),
          const Text("Version: 1.0.0"),
          GestureDetector(
            child: const Card(
              clipBehavior: Clip.antiAlias,
              child: ListTile(
                leading: Icon(Icons.delete),
                title: Text("Delete local data"),
                subtitle: Text("Delete all local shopping lists and items and logout."),
              ),
            ),
            onTap: () {
              SqliteHandler.deleteAllLocalData().then((value) async {
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                        behavior: SnackBarBehavior.floating,
                        content: Text('All local data was deleted!')
                    )
                );
                await FirebaseAuth.instance.signOut();
              });
            },
          ),
          GestureDetector(
            child: const Card(
              clipBehavior: Clip.antiAlias,
              child: ListTile(
                leading: Icon(Icons.logout),
                title: Text("Logout"),
                subtitle: Text("Logout from your account. Local lists remain and can be used with any other account."),
              ),
            ),
            onTap: () async {
              await FirebaseAuth.instance.signOut();
            },
          ),
        ],
      ),
    );
  }
}
